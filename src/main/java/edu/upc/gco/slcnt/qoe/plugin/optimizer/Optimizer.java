package edu.upc.gco.slcnt.qoe.plugin.optimizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSType;
import edu.upc.gco.slcnt.rest.client.qoe.QoEOApi;
import edu.upc.gco.slcnt.rest.client.qoe.model.QoEFeedback;
import edu.upc.gco.slcnt.qoe.plugin.QoEPlugin;
import edu.upc.gco.slcnt.qoe.plugin.rest.model.QoEObject;

public class Optimizer {
	
	private Log logger = LogFactory.getLog(Optimizer.class);
	private QoEPlugin qoePlugin;
	private String URI;
	private QoEOApi qoeApi;
	
	public Optimizer()
	{
		
	}
	
	public Optimizer (QoEPlugin qoe)
	{
		this.qoePlugin = qoe;
		this.qoeApi = new QoEOApi();
	}
	
	/*private ClientHttpRequestFactory getClientHttpRequestFactory()
	{
		int timeout = 2000;
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setConnectTimeout(timeout);
		clientHttpRequestFactory.setConnectionRequestTimeout(timeout);
		clientHttpRequestFactory.setReadTimeout(timeout);
		
		return clientHttpRequestFactory;
	}*/
	
	private void getOptimizerFromCPSR ()
	{
		String optimizerUri = this.qoePlugin.getCpsr().getOptimizerURI(this.qoePlugin.getSliceId().toString(), CPSType.QOE.getValue());
		if (optimizerUri == null)
			logger.error("QoE Optimizer not found in the CPSR!");
		else
			this.qoePlugin.getQoeOpt().setURI(optimizerUri);
	}
	
	private QoEFeedback toQoEFeedBack (QoEObject qoeObject)
	{
		QoEFeedback qoeFB = new QoEFeedback();
		
		qoeFB.setId(qoeObject.getId());
		qoeFB.setValue(qoeObject.getValue());
		
		return qoeFB;
	}
	
	public void sendQoEFeedback (QoEObject qoeObject)
	{
		// Get the QoE-O from the CPSR
		this.getOptimizerFromCPSR();
		
		/*RestTemplate rt = new RestTemplate(this.getClientHttpRequestFactory());
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	
		HttpEntity<QoEObject> qoeUpdate = new HttpEntity<QoEObject>(qoeObject, headers);
		logger.info("PUT: " + this.URI);
		rt.exchange(this.URI, HttpMethod.PUT, qoeUpdate, Void.class);*/
		
		this.qoeApi.sendQoEFeedback(this.URI, this.toQoEFeedBack(qoeObject));
	}
	
	public QoEPlugin getQoePlugin() {
		return qoePlugin;
	}

	public void setQoePlugin(QoEPlugin qoe) {
		this.qoePlugin = qoe;
	}

	public String getURI() {
		return URI;
	}

	public void setURI(String uRI) {
		URI = uRI;
	}
}
