package edu.upc.gco.slcnt.qoe.plugin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@SpringBootApplication
@ComponentScan(basePackages = {"edu.upc.gco.slcnt.qoe.plugin", "edu.upc.gco.slcnt.qoe.plugin.rest.api" , "edu.upc.gco.slcnt.qoe.plugin.rest.configuration"})
public class OpenAPI2SpringBoot implements CommandLineRunner {

	private Log logger = LogFactory.getLog(OpenAPI2SpringBoot.class);
	// Test: Register to the P&P
	//public final String PandP_URI = "http://localhost:60001/plug-and-play-test/pp_management/registration/module/";
	public QoEPlugin qoePlugin = new QoEPlugin();
    
	/*private String loadJson () throws IOException
	{
		String strJson = "";
		List<String> lines = null;
		lines = Files.readAllLines(Paths.get("/QoE-P_v1.json"));
		
		for (String l : lines)
		{
			strJson += l;
		}
		logger.info(strJson);
		return strJson;
    }
    
	private ClientHttpRequestFactory getClientHttpRequestFactory()
	{
		int timeout = 2000;
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setConnectTimeout(timeout);
		clientHttpRequestFactory.setConnectionRequestTimeout(timeout);
		clientHttpRequestFactory.setReadTimeout(timeout);
		
		return clientHttpRequestFactory;
	}
	
    private void registerPlugin () throws IOException
    {
    	
    	RestTemplate rt = new RestTemplate(this.getClientHttpRequestFactory());
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);
    	HttpEntity<String> qoeJson = null;
		
    	qoeJson = new HttpEntity<String>(this.loadJson(), headers);
		
    	try
    	{
    		rt.postForObject(PandP_URI, qoeJson, String.class);
    	}
    	catch (RestClientException rce)
    	{
    		logger.error(rce.getMessage());
    	}
    }*/
    // End Test
	
    @Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exitcode")) {
            throw new ExitException();
        }
        // Test: Register to the P&P
        /*try
        {
        	registerPlugin();
        }
        catch (IOException ioe)
        {
        	ioe.printStackTrace();
        }
        catch (RestClientException rce)
        {
        	rce.printStackTrace();
        }*/
        this.qoePlugin.initPandP();
        // End Test
    }

    public static void main(String[] args) throws Exception {
        new SpringApplication(OpenAPI2SpringBoot.class).run(args);
    }

    class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }

    }

    @Bean
    public WebMvcConfigurer webConfigurer() {
        return new WebMvcConfigurer() {
            /*@Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods("*")
                        .allowedHeaders("Content-Type");
            }*/
        };
    }
    
    @Bean
    public QoEPlugin getQoEPlugin ()
    {
    	return this.qoePlugin;
    }

}
