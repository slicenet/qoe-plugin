package edu.upc.gco.slcnt.qoe.plugin.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import edu.upc.gco.slcnt.qoe.plugin.QoEPlugin;
import edu.upc.gco.slcnt.qoe.plugin.rest.model.QoEConf;
import io.swagger.annotations.ApiParam;

import java.util.Optional;

import javax.validation.Valid;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2018-09-04T18:08:31.775+02:00[Europe/Madrid]")

@Controller
@RequestMapping("${openapi.qoePluginV1.base-path:/}")
public class ConfApiController implements ConfApi {

	//QoEConf qoeConf = new QoEConf();
	private final NativeWebRequest request;
	@Autowired
	private ApplicationContext appContext;

	@org.springframework.beans.factory.annotation.Autowired
	public ConfApiController(NativeWebRequest request) {
		this.request = request;
	}

	@Override
	public Optional<NativeWebRequest> getRequest() {
		return Optional.ofNullable(request);
	}
	
	@Override
	public ResponseEntity<Void> confPlugin(@ApiParam(value = ""  )  @Valid @RequestBody QoEConf qoEConf) {
		
		QoEPlugin qoe = appContext.getBean(QoEPlugin.class);
		qoe.getQoeConf().setSliceId(qoEConf.getSliceId());
		qoe.getQoeConf().setSliceOwner(qoEConf.getSliceOwner());
		qoe.getQoeConf().setRegistryIp(qoEConf.getRegistryIp());
		qoe.getQoeConf().setRegistryPort(qoEConf.getRegistryPort());
		//qoe.getCpsr().setRegistryIp(qoEConf.getRegistryIp());
		//qoe.getCpsr().setRegistryPort(qoEConf.getRegistryPort());
		
		qoe.initQoEPlugin(qoEConf.getSliceId(), qoEConf.getRegistryIp(), qoEConf.getRegistryPort());
		
		// Get the URI of the QoE-Optimizer from the CPSR
		//String optimizerURI = qoe.getCpsr().requestFunction(qoEConf.getSliceId(), CPSType.QOE.getValue());
		// Set URI of the QoE-Optimizer at the Optimizer client instance
		//qoe.getQoeOpt().setURI(optimizerURI);
		
		return new ResponseEntity<>(HttpStatus.OK);
		//return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
	}
}
