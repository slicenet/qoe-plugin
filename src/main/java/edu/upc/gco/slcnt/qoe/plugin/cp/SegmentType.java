package edu.upc.gco.slcnt.qoe.plugin.cp;

public enum SegmentType {
	RAN,
	MEC,
	CORE,
	WAN;
}
