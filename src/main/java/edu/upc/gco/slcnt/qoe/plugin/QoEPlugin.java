package edu.upc.gco.slcnt.qoe.plugin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import edu.upc.gco.slcnt.rest.client.cp.cpsr.model.CPSType;
import edu.upc.gco.slcnt.rest.client.pandp.PandPApi;
import edu.upc.gco.slcnt.qoe.plugin.cp.CPSR;
import edu.upc.gco.slcnt.qoe.plugin.optimizer.Optimizer;
import edu.upc.gco.slcnt.qoe.plugin.rest.model.QoEConf;
import edu.upc.gco.slcnt.qoe.plugin.rest.model.QoEObject;

public class QoEPlugin {
	
	private QoEConf qoeConf;
	private QoEObject qoeObj;
	private Optimizer qoeOpt = null;
	private CPSR cpsr = null;
	private UUID sliceId;
	private Log logger = LogFactory.getLog(QoEPlugin.class);
	private String PandPURI = "";
	private PandPApi pandpApi;
	@Autowired
	private Environment env;
	
	public QoEPlugin ()
	{
		//this.cpsr = new CPSR();
		this.qoeConf = new QoEConf();
		this.qoeObj = new QoEObject();
		//this.PandPURI = env.getProperty("p&p.uri");
		//this.pandpApi = new PandPApi(this.PandPURI);
		
		//this.qoeObj.setId("-1");
		//this.qoeObj.setValue("Unknown");
		//this.qoeOpt = new Optimizer (this);
	}
	
	private String loadJson()
	{
		String strJson = "";
		List<String> lines = null;
		
		try
		{
			lines = Files.readAllLines(Paths.get(env.getProperty("server.conf")));
		}
		catch (IOException e)
		{
			logger.error("Error processing JSON conf file. " + e.getMessage());
		}
		
		for (String l : lines)
		{
			strJson += l;
		}
		logger.info(strJson);
		return strJson;
    }
	
	public void initPandP ()
	{
		this.PandPURI = env.getProperty("p&p.uri");
		this.pandpApi = new PandPApi(this.PandPURI);
		this.pandpApi.registerPlugin(this.loadJson());
		
		initQoEPlugin (env.getProperty("slicenetID"), env.getProperty("cpsrIP"), env.getProperty("cpsrPort"));
	}
	
	// This operation initializes the QoE Plugin after the initial configuration is received from the Plug & Play --> ConfAPI Deprecated, this operation is now called locally
	public void initQoEPlugin (String sliceId, String registryIP, String registryPort)
	{
		this.cpsr = new CPSR(registryIP, registryPort);
		
		this.qoeObj.setId(sliceId);
		this.qoeObj.setValue("Unknown");
		
		this.qoeOpt = new Optimizer (this);
		
		this.sliceId = UUID.fromString(sliceId);
		
		// Moved to the Optimizer: The CPSR is contacted at every request received
		/*
		//this.initOptimizer (this.cpsr.getBaseUrl(), sliceId);
		String optimizerUri = this.cpsr.getOptimizerURI(sliceId, CPSType.QOE.getValue());
		if (optimizerUri == null)
			logger.error("QoE Optimizer not found in the CPSR!");
		else
			this.qoeOpt.setURI(optimizerUri);
		*/
	}
	
	// Optimization
	public void sendQoEFeedback (QoEObject qoeObj)
	{
		this.qoeOpt.sendQoEFeedback(qoeObj);
	}

	public QoEConf getQoeConf() {
		return qoeConf;
	}

	public void setQoeConf(QoEConf qoeConf) {
		this.qoeConf = qoeConf;
	}

	public QoEObject getQoeObj() {
		return qoeObj;
	}

	public void setQoeObj(QoEObject qoeObj) {
		this.qoeObj = qoeObj;
	}

	public Optimizer getQoeOpt() {
		return qoeOpt;
	}

	public void setQoeOpt(Optimizer qoeOpt) {
		this.qoeOpt = qoeOpt;
	}

	public CPSR getCpsr() {
		return cpsr;
	}

	public void setCpsr(CPSR cpsr) {
		this.cpsr = cpsr;
	}

	public UUID getSliceId() {
		return sliceId;
	}

	public void setSliceId(UUID sliceId) {
		this.sliceId = sliceId;
	}	
}
