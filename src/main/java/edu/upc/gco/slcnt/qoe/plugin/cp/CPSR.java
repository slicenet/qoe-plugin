package edu.upc.gco.slcnt.qoe.plugin.cp;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upc.gco.slcnt.rest.client.cp.cpsr.CPSRApi;

public class CPSR {
	
	private String registryIp;
	private String registryPort;
	private String baseUrl;
	private Log logger = LogFactory.getLog(CPSR.class);
	private CPSRApi cpsrClient;
	
	public CPSR()
	{
		
	}
	
	public CPSR (String ip, String port)
	{
		this.registryIp = ip;
		this.registryPort = port;
		this.baseUrl = "http://" + this.registryIp + ":" + this.registryPort;
		this.cpsrClient = new CPSRApi();
	}
	
	public String getOptimizerURI (String sliceId, String cpsType)
	{
		ResponseEntity<String> response = this.cpsrClient.getCPSInstances(this.baseUrl, sliceId, cpsType);
		String optimizerUri = null;
		switch (response.getStatusCodeValue())
		{
			case 200:
				ObjectMapper mapper = new ObjectMapper();
				try {
					JsonNode root = mapper.readTree(response.getBody());
					if (root.isArray())
						optimizerUri = root.get(0).get("uri").asText();
				} catch (IOException e) {
					logger.error("Error processing the request: " + e.getMessage());
				}
				break;
			case 404:
			default:
				break;
		}
		
		logger.info("QoE Optimizer URI: " + optimizerUri);
		return optimizerUri;
	}

	public String getRegistryIp() {
		return registryIp;
	}

	public void setRegistryIp(String registryIp) {
		this.registryIp = registryIp;
	}

	public String getRegistryPort() {
		return registryPort;
	}

	public void setRegistryPort(String registryPort) {
		this.registryPort = registryPort;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public CPSRApi getCpsrClient() {
		return cpsrClient;
	}

	public void setCpsrClient(CPSRApi cpsrClient) {
		this.cpsrClient = cpsrClient;
	}
}
